package com.example.org.belhard6_file2;

import java.io.File;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


public class MainActivity extends ListActivity implements View.OnClickListener {

    private static final int DELETE = 1;
    private static final String FILE_NAME = "fileName";

    private ArrayAdapter<String> mAdapter;  //если не зенаешь что значит прификс 'm', то не исопльзуй его. да и вообще он нафиг не нужен. //не знаю, тут явно копи паст от меня



    private Button addFileButton;

    private File appsDir;
    private File[] files;
    private String[] fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //FIXME: если у тебя виджет (баттон) используется в одном месте, то можно сделать так:
        /*
            findViewById(R.id.addFileButton).setOnClickListener(this);;
         */
        addFileButton = (Button)findViewById(R.id.addFileButton);
        addFileButton.setOnClickListener(this);

        updateListFiles();

    }

    protected void onRestart() {
        super.onRestart();

        updateListFiles();

    }

    public void updateListFiles() {

        appsDir = getFilesDir();

        files = appsDir.listFiles();

        fileName = new String[appsDir.list().length];



        //Ну да, мб я не заметил что там что-т оприсваиваешь. Мб первый вариант был и лучше. Ну главное что занешь про этот цикл

        int z = 0;

        for(File tempFile: files){

            fileName[z] = tempFile.getName();
            z ++;
        }


        mAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, fileName);
        setListAdapter(mAdapter);
        getListView().setOnCreateContextMenuListener(this);

    }




    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        //твой код
        Intent intent = new Intent(MainActivity.this, EditFile.class);
        intent.putExtra(FILE_NAME, fileName[(int) id]);
        startActivity(intent);

        //мой код
//        EditFile.start(this, fileName[(int) id]);

    }

    public void onCreateContextMenu(ContextMenu menu, View v,
        ContextMenu.ContextMenuInfo menuInfo) {

            super.onCreateContextMenu(menu, v, menuInfo);

            menu.add(0, DELETE, 0, getString(R.string.delete_file));

    }

    //FIXME: зачем тут пишешь через одну строку все?
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

            switch (item.getItemId()) {

                case DELETE:

                    File f = new File(appsDir.getAbsolutePath(), fileName[(int) acmi.id]);
                    f.delete();
                    break;

            }

            updateListFiles();
            return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(MainActivity.this, EditFile.class);
        startActivity(intent);

    }

}
