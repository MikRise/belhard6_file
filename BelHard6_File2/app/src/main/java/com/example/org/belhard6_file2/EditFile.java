package com.example.org.belhard6_file2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


//FIXME: Желательно чтобы названия у активити заканчивалось на примерно так EditFileActivity
public class EditFile extends Activity implements View.OnClickListener {

    private static final String FILE_NAME = "fileName";

    private EditText textFileName;
    private EditText textFile;
    private Button saveFile;
    private Button newFile;


//TODO: расскоменть 
//    public static void start(Context context, String fileName) {
//        Intent intent = new Intent(context, EditFile.class);
//        intent.putExtra(FILE_NAME, fileName);
//
//        context.startActivity(intent);
//    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_file);

        textFileName = (EditText)findViewById(R.id.textFileName);
        textFile = (EditText)findViewById(R.id.textFile);

        saveFile = (Button)findViewById(R.id.saveFile);
        newFile = (Button)findViewById(R.id.newFile);
        saveFile.setOnClickListener(this);
        newFile.setOnClickListener(this);

        Intent intent = getIntent();


        //FIXME данное условие можно заменить на более короткое: if( !intent.hasExtra(FILE_NAME) ) {}
        //FIXME все это условие лучше вынести в отдельные метод. Так станет логичнее и читабельнее
        if(intent.getStringExtra(FILE_NAME) != null){

            String fileName = intent.getStringExtra(FILE_NAME);

            textFileName.setText( fileName.substring(0, fileName.length() - 4));

            try {
                InputStream inStream = openFileInput(fileName);

                if (inStream != null) {
                    InputStreamReader tmp =
                            new InputStreamReader(inStream);
                    BufferedReader reader = new BufferedReader(tmp);
                    String str;
                    StringBuffer buffer = new StringBuffer();

                    while ((str = reader.readLine()) != null) {
                        buffer.append(str);
                    }

                    inStream.close();
                    textFile.setText(buffer.toString());
                }
            }
            catch (Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Exception: " + t.toString(), Toast.LENGTH_LONG)
                        .show();
            }

        }

    }

    //FIXME: я уже писал что текст который отображается в ЮАЙ выноси в ресурсы. Это надо делать для логализации приложений.
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case  R.id.saveFile :

                try {

                    OutputStreamWriter outStream = new OutputStreamWriter(
                        openFileOutput(textFileName.getText().toString() + ".txt", 0));

                    outStream.write(textFile.getText().toString());

                    outStream.close();

                    Toast.makeText(getApplicationContext(), "файл сохранен", Toast.LENGTH_LONG).show();

                }

                catch (Throwable t) {

                     Toast.makeText(getApplicationContext(), "ошибка записи файла", Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.newFile :

                textFileName.setText("");

                textFile.setText("");

                Toast.makeText(getApplicationContext(), "новый файл", Toast.LENGTH_LONG).show();

                break;

        }

    }
}
